package de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.dbo.Employee;
import de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.service.EmployeeService;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenEmployeeObject_whenCreateEmployee_thenReturnSavedEmployee () throws Exception {
        // given - precondition or setup
        Employee employee = Employee.builder()
                .firstName("Ramesh")
                .lastName("Fadatare")
                .email("ramesh@gmail.com")
                .build();
        given(employeeService.saveEmployee(any(Employee.class)))
                .willAnswer((invocation)-> invocation.getArgument(0));

        // when - action or behaviour that we are going test
        ResultActions response = mockMvc.perform(post("/api/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee)));

        // then - verify the result or output using assert statements
        response.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName",
                        is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName",
                        is(employee.getLastName())))
                .andExpect(jsonPath("$.email",
                        is(employee.getEmail())));

    }

    // JUnit test for get all Employees REST API
    @Test
    public void givenListOfEmployees_whenGetAllEmployees_thenReturnEmployeesList() throws Exception {
        // Given Precondition or Setup
        List<Employee> listOfEmployees = new ArrayList<>();
        listOfEmployees.add(Employee.builder().firstName("LiLi").lastName("Minlo").email("lili@dotcomfly.com").build());
        listOfEmployees.add(Employee.builder().firstName("David").lastName("Minlo").email("david@dotcomfly.com").build());
        listOfEmployees.add(Employee.builder().firstName("marie").lastName("Minlo").email("marie@dotcomfly.com").build());
        given(employeeService.getAllEmployee()).willReturn(listOfEmployees);

        // When - action or the behavior that we are going to test
        final ResultActions response = mockMvc.perform(get("/api/employees"));

        // Then verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()",
                        is(listOfEmployees.size())));


    }

    // Positive scenario - valid employee id
    // JUnit test for GET employee by id REST API
    @Test
    public void givenEmployeeId_whenGetEmployeeById_thenReturnEmployeeObject() throws Exception {
        // given - precondition or setup
        long employeeId = 1L;
        Employee employee = Employee.builder()
                .firstName("LiLi")
                .lastName("Minlo")
                .email("lili@dotcomfly.com")
                .build();
        given(employeeService.getEmployeeById(employeeId)).willReturn(Optional.of(employee));

        // When - action or the behaviour that we are going test
        final ResultActions response = mockMvc.perform(get("/api/employees/{id}", employeeId));

        // then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName",
                        is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName",
                        is(employee.getLastName())))
                .andExpect(jsonPath("$.email",
                        is(employee.getEmail())));
    }

    // Negative scenario - valid employee id
    // JUnit test for GET employee by id REST API
    @Test
    public void givenInvalidEmployeeId_whenGetEmployeeById_thenReturnEmpty() throws Exception {
        // given - precondition or setup
        long employeeId = 1L;
        Employee employee = Employee.builder()
                .firstName("LiLi")
                .lastName("Minlo")
                .email("lili@dotcomfly.com")
                .build();
        given(employeeService.getEmployeeById(employeeId)).willReturn(Optional.empty());

        // When - action or the behaviour that we are going test
        final ResultActions response = mockMvc.perform(get("/api/employees/{id}", employeeId));

        // then - verify the output
        response.andExpect(status().isNotFound())
                .andDo(print());
    }

    //JUnit test for update employee REST API - Positive scenario
    @Test
    public void givenUpdatedEmployee_whenUpdateEmployee_thenReturnUpdateEmployeeObject() throws Exception {
        // Given - precondition or setup
        long employeeId = 1L;
        Employee savedEmployee = Employee.builder()
                .firstName("JoJo")
                .lastName("Minlo")
                .email("jojo@mailto.com")
                .build();

        Employee updatedEmployee = Employee.builder()
                .firstName("David")
                .lastName("Minlo")
                .email("davido@minlo.com")
                .build();
        given(employeeService.getEmployeeById(employeeId))
                .willReturn(Optional.of(savedEmployee));
        given(employeeService.updateEmployee(any(Employee.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));

        // When - action or the behaviour that wu are going test
        final ResultActions response = mockMvc.perform(put("/api/employees/{id}", employeeId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedEmployee)));

        // Then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.firstName",
                        is(updatedEmployee.getFirstName())))
                .andExpect(jsonPath("$.lastName",
                        is(updatedEmployee.getLastName())))
                .andExpect(jsonPath("$.email",
                        is(updatedEmployee.getEmail())));

    }

    // JUnit test for update employee REST API - negative scenario
    @Test
    public void givenUpdatedEmployee_whenUpdateEmployee_thenReturn404() throws Exception {
        // Given - precondition or setup

        long employeeId = 1L;
        Employee savedEmployee = Employee.builder()
                .firstName("JoJo")
                .lastName("Minlo")
                .email("jojo@mailto.com")
                .build();

        Employee updatedEmployee = Employee.builder()
                .firstName("David")
                .lastName("Minlo")
                .email("davido@minlo.com")
                .build();
        given(employeeService.getEmployeeById(employeeId))
                .willReturn(Optional.empty());
        given(employeeService.updateEmployee(any(Employee.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));

        // When - action or the behaviour that wu are going test
        final ResultActions response = mockMvc.perform(put("/api/employees/{id}", employeeId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedEmployee)));

        // Then - verify the output
        response.andExpect(status().isNotFound())
                .andDo(print());
    }


    // JUnit test for delete employee REST API
    @Test
    public void givenEmployeeId_whenDeleteEmployee_thenReturn200() throws Exception {
        // Given - precondition or setup
        long employeeId = 1L;
        willDoNothing().given(employeeService).deleteEmployee(employeeId);

        // When - action or the behaviour that we are going test
        final ResultActions response = mockMvc.perform(delete("/api/employees/{id}", employeeId));

        // Then - verify the output
        response.andExpect(status().isOk()).andDo(print());

    }

    /**
     * We are using @MockBean annotation to add mock abjects to the spring application context. The
     * mock will replace any existing bean of the same type in the application context.
     *
     * The @MockBean annotation tells Spring to create a mock instance of EmployeeService and add it
     * to the application context so that it's injected into EmployeeController. We have a handle on it in
     * the test so that we can define its behaviour before running each test.
     *
     * Note that we are using MockMvc class to make REST API calls.
     *
     * We are using ResultActions class to handle the response of the REST API.
     *
     * We are using Mockito to stub the method calls.
     */

}