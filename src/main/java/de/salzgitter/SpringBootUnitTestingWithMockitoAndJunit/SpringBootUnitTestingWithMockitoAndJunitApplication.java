package de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootUnitTestingWithMockitoAndJunitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootUnitTestingWithMockitoAndJunitApplication.class, args);
	}

}
