package de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.dbo;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "FirstName", nullable = false)
    private String firstName;

    @Column(name = "LastName", nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    /**
     * Note that we are using Lombok annotations to reduce the boilerplate code.
     * @Entity annotation is used to mark the class a persistent Java class.
     *
     * @Table annotation is used to provide the details of table that this entity will mapped to.
     *
     * @Id annotation is used to define the primary key.
     *
     * @GeneratedValue annotation is used to define the primary key generation strategy
     * In the above case, we have declared the primary key to be an Auto Increment field
     *
     */
}
