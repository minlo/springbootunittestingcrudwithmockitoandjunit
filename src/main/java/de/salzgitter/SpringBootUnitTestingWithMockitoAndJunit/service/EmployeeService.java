package de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.service;

import de.salzgitter.SpringBootUnitTestingWithMockitoAndJunit.dbo.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Employee saveEmployee(Employee employeeDbo);

    List<Employee> getAllEmployee();

    Optional<Employee> getEmployeeById(long id);

    Employee updateEmployee(Employee updatedEmployee);

    void deleteEmployee(long id);

}
